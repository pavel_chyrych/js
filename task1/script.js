var screenEl = document.getElementById('screen')

var result=0;
var curent = 0
var flagCurentEmpty = true
var operationKey = ''


var onNum = function(num) {
  // Логгирование
  curent=curent*10+num
  flagCurentEmpty = false
  console.log(curent)
  screenEl.innerHTML = curent
}

var calc = function(){
  if(operationKey==''){
    result=curent
    curent=0
    flagCurentEmpty=true
  }
  else{
    if(!flagCurentEmpty){
      if(operationKey=='+'){
        result+=curent
      }
      if(operationKey=='-'){
        result-=curent
      }
      if(operationKey=='*'){
        result*=curent
      }
      if(operationKey=='/'){
        result/=curent
      }
      curent=0
      flagCurentEmpty=true
      operationKey = ''
    }
    console.log(result)
    screenEl.innerHTML = result
  }
}

var onAdd = function() {
  calc();
  operationKey = '+'
}

var onDiv = function() {
  calc();
  operationKey = '/'
}

var onMul = function() {
  calc();
  operationKey = '*'
}

var onSub = function() {
  calc();
  operationKey = '-'
}

var onCompute = function(){
  calc();
  operationKey = ''
}

var onReset = function() {
  screenEl.innerHTML = '0'
  curent=0
  flagCurentEmpty=true
  result=0
  operationKey = ''
}